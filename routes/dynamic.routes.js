const express = require('express');
const { QueryTypes } = require('sequelize');
const router = express.Router();
const sequelize = require('../utils/db');

router.get('/', async (req, res) => {
  // send all categories
  const [results, metadata] = await sequelize.query('SELECT * FROM `categories`');
  res.status(200).json({
    status: 'success',
    total: results.length,
    data: results
  });
});

router.get('/:categorySlug', async (req, res) => {
  //Find the category and send its details
  const results = await sequelize.query(
    'SELECT id, name, display_type, description FROM `brands`',
    { type: QueryTypes.SELECT }
  );
  const sliced = results.slice(0, 10);
  res.status(200).json({
    status: 'success',
    total: results.length,
    results: sliced.length,
    data: sliced
  });
});

router.get('/:categorySlug/:brandSlug', async (req, res) => {
  //Find products and send its details
  let query = 'SELECT id from `brands` WHERE slug="allen-solly-gift-vouchers"';
  const brandId = await sequelize.query(query, { type: QueryTypes.SELECT });
  query =
    'SELECT id, display_type, product_guid, name, price, expiry_date, available_qty FROM `products` WHERE brand_id="' +
    brandId[0].id +
    '"';
  const results = await sequelize.query(query, { type: QueryTypes.SELECT });
  res.status(200).json({
    status: 'success',
    data: results
  });
});

module.exports = router;
