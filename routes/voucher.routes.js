const axios = require('axios');
const AppError = require('../utils/AppError');

const voucherHandler = async (req, res, next) => {
  try {
    const result = await axios({
      method: 'post',
      url: 'http://communication.gyftr.net/API/v1/voucher/details ',
      headers: {
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({
        voucher: 'SOME-VOUCHER-CODE'
      })
    });

    if (result && result.data) {
      return res.status(200).json({
        status: 'success',
        data: result.data
      });
    }
  } catch (err) {
    next(new AppError('Something went wrong', 500));
  }
};

module.exports = {
  voucherHandler
};
