const router = require('express').Router();
const authController = require('../controller/auth.controller');
const dbpool = require('../utils/database');

router.post('/signup', authController.signupHandler);
router.post('/login', authController.loginHandler);
router.get('/me', authController.authCheck, authController.profileHandler);

router.post('/authentication', async (req, res, next) => {
  //   const [result, metadata] = await dbpool.execute('SELECT * FROM users');
  const { username, email, phone, name, display_type, program_type } = req.body;
  const [result, metadata] = await dbpool.execute(
    `CALL user_information('${username}', '${email}', '${phone}', '${name}', '${display_type}', '${program_type}')`
  );
  //   console.log(result);
  res.json({ result: result[0][0] });
});

module.exports = router;
