const express = require('express');
const router = express.Router();
const { Sequelize } = require('sequelize');
const { checkQuantity } = require('../utils/checkQuantity');

/**
 * Function Fetch client -
 * brings all the clients from DB by calling
 * a stored procedure fetch_clients()
 */

async function fetchClients() {
  try {
    const sequelize = new Sequelize('client_db', 'root', 'devroot', {
      dialect: 'mysql',
      host: 'localhost'
    });

    const result = await sequelize.query('CALL fetch_clients()');
    sequelize.close();
    return result;
  } catch (err) {
    console.error(err);
    throw err;
  }
}

router.patch('/', async (req, res, next) => {
  try {
    // FETCH ALL CLIENTS
    const clients = await fetchClients();
    const clinet_products = [];
    const products_updated = [];

    for (const client of clients) {
      // CONNECT TO HIS DB
      const connetion_r = await new Sequelize(client.db_name, client.db_user, client.db_password, {
        dialect: 'mysql',
        host: 'localhost'
      });

      const connetion_wr = await new Sequelize(client.db_name, client.db_user, client.db_password, {
        dialect: 'mysql',
        host: 'localhost',
        dialectOptions: {
          multipleStatements: true
        }
      });

      // FOR EACH CLIENT FETCH products
      const products = await connetion_r.query('CALL fetch_products()');

      // FOR EACH Product Fetch its Quantity - update its Quantity
      const updateQueries = [];
      for (const product of products) {
        const quantity = await checkQuantity(
          product.product_guid,
          client.buyer_user,
          client.buyer_password
        );

        if (quantity) {
          // check if there is a change in quantity
          if (quantity.AvailableQuantity != product.available_qty) {
            // 2. Create Update quantity Query
            const query = `UPDATE products SET available_qty=${quantity.AvailableQuantity} WHERE product_guid='${product.product_guid}';`;
            updateQueries.push(query);
          }
        }
      }

      // FIRE UPDATE QUANTITY QUERY
      const result = await connetion_wr.query(updateQueries.join(''));
      products_updated.push(result[0][0]);

      clinet_products.push({
        client_name: client.db_name,
        updated: products_updated
      });

      await connetion_r.close();
      await connetion_wr.close();
    }

    res.status(200).json({
      clients,
      products: clinet_products
    });
  } catch (err) {
    res.status(500).json({
      message: err.message || 'Something went wrong',
      error: err
    });
  }
});

module.exports = router;
