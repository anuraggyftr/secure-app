const express = require('express');
const { QueryTypes } = require('sequelize');
const router = express.Router();
const sequelize = require('../utils/db');

router.get('/:categorySlug?/:brandSlug?', async (req, res) => {
  const { categorySlug, brandSlug } = req.params;
  let query;
  if (categorySlug && brandSlug) {
    // send products associated with the category -> brand -> products
    query =
      'SELECT id from `categories` WHERE slug="' + categorySlug + '" AND display_type="WEBSITE"';

    const cotegoryId = await sequelize.query(query, { type: QueryTypes.SELECT });
    if (!cotegoryId) return res.status(400).json({ error: 'category not found' });

    query =
      'SELECT id, display_type, product_guid, name, price, expiry_date, available_qty FROM `products` WHERE brand_id="' +
      brandId[0].id +
      '" AND display_type="WEBSITE"';
    const results = await sequelize.query(query, { type: QueryTypes.SELECT });
    res.status(200).json({
      status: 'success',
      data: results
    });
  } else if (categorySlug) {
    // send brands listed in the category
    query = `CALL findCategory('${categorySlug}')`;
    const [result, metadata] = await sequelize.query(query);
    res.status(200).json({
      status: 'success',
      data: result
    });
  } else {
    // send all categories
    query = 'CALL `selectCategories`()';
    const results = await sequelize.query(query);
    res.status(200).json({
      status: 'success',
      count: results.length,
      results
    });
  }
});

module.exports = router;
