const router = require('express').Router();
const AppError = require('../utils/AppError');
const Cryptr = require('cryptr');

const key = 'MySuper_SECRET_48bit-LoNg_#ULTRA-_-%SECURE%@key;';
const cryptr = new Cryptr(key);

//************ TO DO ***************
// 1) encrypt the reqbody from client side code

// 2) send it via axios from client side to server

// 3) ON server, at /decrypt route decrypt it and send back the decrypted payload

router.post('/decrypt', async (req, res, next) => {
  try {
    const decryptedString = await cryptr.decrypt(req.body.encrypted);
    const data = JSON.parse(decryptedString);
    res.status(200).json({
      status: 'success',
      decryptedString,
      data
    });
  } catch (err) {
    if (err) next(new AppError('Invalid Encrypted Data', 400));
  }
});

router.post('/', (req, res) => {
  const jsonString = JSON.stringify(req.body);
  const encryptedString = cryptr.encrypt(jsonString);
  res.status(200).json({
    status: 'success',
    body: req.body,
    jsonString,
    data: encryptedString
  });
});

module.exports = router;
