const path = require('path');
const express = require('express');
require('express-async-errors');
const morgan = require('morgan');
const dotenv = require('dotenv');
const hpp = require('hpp');
const xss = require('xss-clean');
const helmet = require('helmet');
const rateLimit = require('express-rate-limit');
const slowDown = require('express-slow-down');
const { voucherHandler } = require('./routes/voucher.routes');

dotenv.config({ path: './.env' });

const app = express();

app.use(helmet());
const limiter = rateLimit({
  windowMs: 30 * 60 * 1000, // 30 minutes
  max: 100, // limit each IP to 100 requests per windowMs
  message: 'Too many requests, please try again after an hour'
});

app.use(morgan('dev'));

// Body Parsing
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Security Middlewares
app.use(hpp());
app.use(xss());

// static directory
app.use(express.static(path.join(__dirname, '/public')));

app.get('/', (req, res, next) => {
  res.send('WELCOME TO AWESOME APP');
});

app.use('/promotions', require('./routes/dynamic.routes'));
app.use('/website', require('./routes/website.routes'));
app.use('/api/v1/user', limiter, require('./routes/user.routes'));
app.use('/api/v1/products', require('./routes/product.routes'));
app.use('/api/v1/test-encryption', require('./routes/encryption.routes'));

const rateLimiter = rateLimit({
  windowMs: 2 * 60 * 1000, // 2 minutes
  max: 10, // limit each IP to 10 requests per windowMs
  message: 'Too many requests, please try again after an hour'
});

const speedLimiter = slowDown({
  windowMs: 5 * 60 * 1000, // 5 minutes
  delayAfter: 10, // allow 10 requests per 5 minutes, then...
  delayMs: 500 // begin adding 500ms of delay per request above 10:
  // request # 11 is delayed by  500ms
  // request # 12 is delayed by 1000ms
  // request # 13 is delayed by 1500ms
  // etc.
});

app.all('/api/v1/voucher-details', speedLimiter, rateLimiter, voucherHandler);

app.all('*', (req, res, next) => {
  res.status(404).send('Resource Not found');
});

app.use((err, req, res, next) => {
  res.status(err.statusCode || 500).json({
    status: 'fail',
    message: err.message
  });
});

module.exports = app;
