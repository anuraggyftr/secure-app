const mysql = require('mysql2');

const dbpool = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: 'devroot',
  database: 'testdb'
});

module.exports = dbpool.promise();
