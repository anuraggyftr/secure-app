const axios = require('axios');

// CONFIG
const vouchagram = {
  //staging credential
  BASE_URL: 'https://catalog.vouchagram.net/',
  USER_ID: '3627544B-ED62-4483-A6EE-8C7BBA987308',
  PASSWORD: '7757FE887C5',
  DefaultTemplate: '10',

  P_USER_ID: '3627544B-ED62-4483-A6EE-8C7BBA987308',
  P_PASSWORD: '7757FE887C5',
  P_DefaultTemplate: '10',

  QUERY_API_URL: 'https://pos-staging.vouchagram.net/service/restserviceimpl.svc/',
  QUERY_MERCHANTUID: '49F79EB8-061D-4A6F-B7FA-E5DC1088DBBE',
  QUERY_PASSWORD: 'DpWB+dGrxKnrWEEXPUZC/A==',
  QUERY_SHOPCODE: 'webtest'
};

async function checkQuantity(guid, buyerId, buyerPass) {
  try {
    if (!guid || !buyerId || !buyerPass) return null;
    const url = `${vouchagram.BASE_URL}EPService.svc/VoucherQuantity?buyerGUID=${encodeURIComponent(
      buyerId
    )}&password=${encodeURIComponent(buyerPass)}&productguid=${encodeURIComponent(guid)}`;

    const result = await axios.get(url);
    const res_data = result.data;

    if (
      res_data.VoucherQuantityResult &&
      res_data.VoucherQuantityResult[0].ResultType == 'SUCCESS'
    ) {
      return res_data.VoucherQuantityResult[0].QuantityResponse[0];
    }

    return null;
  } catch (err) {
    throw err;
  }
}

module.exports.checkQuantity = checkQuantity;
