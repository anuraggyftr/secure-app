const { Sequelize } = require("sequelize");

const sequelize = new Sequelize("awesomedb", "root", "devroot", {
  dialect: "mysql",
  host: "localhost",
});

module.exports = sequelize;
