const crypto = require('crypto');
const path = require('path');
const fs = require('fs');
const { generateKeyPairSync } = crypto;
const { writeFileSync } = fs;

function generateKeys() {
  const { privateKey, publicKey } = generateKeyPairSync('rsa', {
    modulusLength: 4096,
    publicKeyEncoding: {
      type: 'pkcs1',
      format: 'pem'
    },
    privateKeyEncoding: {
      type: 'pkcs1',
      format: 'pem',
      cipher: 'aes-256-cbc',
      passphrase: 'super_sEcReT%KeY%'
    }
  });

  writeFileSync('private.pem', privateKey);
  writeFileSync('public.pem', publicKey);
}

const encryptWithRsaPublicKey = function (toEncrypt, path_to_public_pem) {
  const absolutePath = path.resolve(path_to_public_pem);
  const publicKey = fs.readFileSync(absolutePath, 'utf8');
  const buffer = Buffer.from(toEncrypt);
  const encrypted = crypto.publicEncrypt(publicKey, buffer);
  return encrypted.toString('base64');
};

const decryptWithRsaPrivateKey = function (toDecrypt, path_to_private_pem) {
  const absolutePath = path.resolve(path_to_private_pem);
  const privateKey = fs.readFileSync(absolutePath, 'utf8');
  const buffer = Buffer.from(toDecrypt, 'base64');
  const decrypted = crypto.privateDecrypt(privateKey, buffer);
  return decrypted.toString('utf8');
};



const  algorithm = 'aes-256-gcm',
const  key = 'super_sEcReT%KeY%',
const  iv = crypto.randomBytes(16);

function encrypt(text) {
  var cipher = crypto.createCipheriv(algorithm, key, iv)
  var encrypted = cipher.update(text, 'utf8', 'hex')
  encrypted += cipher.final('hex');
  var tag = cipher.getAuthTag();
  return {
    content: encrypted,
    tag: tag
  };
}

function decrypt(encrypted) {
  var decipher = crypto.createDecipheriv(algorithm, key, iv)
  decipher.setAuthTag(encrypted.tag);
  var dec = decipher.update(encrypted.content, 'hex', 'utf8')
  dec += decipher.final('utf8');
  return dec;
}

module.exports = {
  encryptWithPublic: encryptWithRsaPublicKey,
  decryptWithPrivate: decryptWithRsaPrivateKey,
  generateKeys,
  encrypt,
  decrypt
};

// const aes256 = require('aes256');

// var key = 'my passphrase';
// var plaintext = 'my plaintext message';

// // var encrypted = aes256.encrypt(key, plaintext);
// // var decrypted = aes256.decrypt(key, encrypted);

// var cipher = aes256.createCipher(key);

// var encrypted = cipher.encrypt(plaintext);
// var decrypted = cipher.decrypt(encrypted);

// console.log(cipher, decrypted);