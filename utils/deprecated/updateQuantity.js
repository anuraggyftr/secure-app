const { Sequelize } = require('sequelize');
const { checkQuantity } = require('../checkQuantity');

async function updateQty() {
  try {
    const user_products = [];
    const products_updated = [];
    const clients = await fetchClients();

    await Promise.all(
      clients.map(async client => {
        const connetion = new Sequelize(client.db_name, client.db_user, client.db_password, {
          dialect: 'mysql',
          host: 'localhost'
        });

        /********FETCH PRODUCT********/
        let products = await connetion.query(
          "SELECT * FROM products WHERE `status` = 'A' AND `expiry_date` > NOW()"
        );
        products = products[0];
        /********For Each product********/
        await Promise.all(
          products.map(async product => {
            // 1. Check Quantity
            const quantity = await checkQuantity(
              product.product_guid,
              client.buyer_user,
              client.buyer_password
            );

            console.log('------>', quantity);
            if (quantity) {
              // check if there is a change in quantity
              if (quantity.AvailableQuantity != product.available_qty) {
                // 2. Update quantity into DB
                const query = `UPDATE products SET available_qty=${quantity.AvailableQuantity} WHERE product_guid='${product.product_guid}';`;
                const result = await connetion.query(query);
                products_updated.push(result[0]);
              }
            }
          })
        );

        user_products.push({
          user: client.buyer_user,
          database: client.db_name,
          updated: products_updated
        });

        connetion.close();
      })
    );

    return user_products;
  } catch (err) {
    throw err;
  }
}

module.exports.updateQty = updateQty;
