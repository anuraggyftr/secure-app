const crypto = require('crypto');

// put in config file
const algorithm = 'aes-256-gcm';
const key = 'MySuper_SECRET_48bit-LoNg_#ULTRA'; // key must not be more than 32 chars long
const iv = crypto.randomBytes(16).toString('hex'); // change everytime

/**
 * encrypt() function - takes the request payload as an object
 * @param {1} payload : Object
 * @returns {content: encrypted} object
 * @throws Error: if no payload or non-object type payload provided
 */

function encrypt(payload) {
  // Validation logics
  if (!payload || typeof payload !== 'object') throw new Error('Invalid payload');
  const cipher = crypto.createCipheriv(algorithm, key, iv);
  let encrypted = cipher.update(JSON.stringify(payload), 'utf8', 'hex');
  encrypted += cipher.final('hex');
  const tag = cipher.getAuthTag();
  return JSON.stringify({
    content: encrypted,
    tag: tag.toString('hex')
  });
}

/**
 * decrypt() function - takes the encrypted string as input
 * @param {1} payload : String
 * @returns {decrypted}: object
 * @throws Error : if no parmeter provided or Invalid string provided
 */

function decrypt(encrypted) {
  if (!encrypted || typeof encrypted !== 'string') throw new Error('Invalid Parameter');
  encrypted = JSON.parse(encrypted);
  const decipher = crypto.createDecipheriv(algorithm, key, iv);
  decipher.setAuthTag(Buffer.from(encrypted.tag, 'hex'));
  let dec = decipher.update(encrypted.content, 'hex', 'utf8');
  dec += decipher.final('utf8');
  return JSON.parse(dec);
}

// const encrypted = encrypt({ name: 'John doe' });
// console.log(encrypted);
// console.log(decrypt(encrypted));

module.exports = {
  encrypt,
  decrypt
};
