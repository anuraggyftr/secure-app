class AppError extends Error {
  constructor(message, errCode) {
    super(message);
    this.statusCode = errCode;
  }
}

module.exports = AppError;
