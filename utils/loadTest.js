const axios = require('axios');
const chalk = require('chalk');
const { Sequelize } = require('sequelize');

async function loadTest() {
  try {
    for (let i = 0; i < 500; i++) {
      const ExternalOrderId = `gyft_${Date.now()}_${i}`;
      // HIT external API
      const result = await axios({
        method: 'post',
        url: 'http://communication.gyftr.net/API/v1/voucher/encrypdata ',
        headers: {
          username: 'ZVBPNPCHVMBUAQTZYOWPLTXVWXWYERDS',
          password: ']soLj$si!x6IL![KP~rkQ^sXG^hT3yJS',
          'Content-Type': 'application/json'
        },
        data: JSON.stringify({
          TemplateId: 155,
          ExternalOrderId,
          ProductGuid: 'b829a455-8bb1-40c5-a7ce-3d7eb5457172',
          Quantity: 1,
          CustomerFName: 'Chandan',
          CustomerMName: '',
          CustomerLName: 'Singh',
          EmailTo: 'chandan@gyftr.com',
          MobileNo: '9518388801',
          CommunicationMode: '5',
          EmailSubject: 'Here is your gift voucher',
          EmailCC: '',
          DynamicVars: {
            GIFT_MESSAGE:
              'On your Birthday, I hope that you get nothing but the best. Have a fantastic year ahead!',
            GIFT_BANNER: 'http://images.gyftr.com/emailer/occassion/birthday_eDM.jpg',
            PROGRAM_NAME_VOUCHER: 'GYFTR',
            SENDER_NAME: 'Deepankar'
          }
        })
      });

      // console.log(result.data.encryp.data);
      // SAVE response
      if (result.data && result.data.encryp) {
        await saveToDB(ExternalOrderId, JSON.stringify({ data: result.data.encryp.data }));
      }
    }
  } catch (err) {
    console.log(err);
  }
}

async function saveToDB(ExternalOrderId, EncryptedData) {
  const connetion = await new Sequelize('load_test', 'anurag', '123456', {
    dialect: 'mysql',
    host: 'localhost'
  });

  return await connetion.query(
    `INSERT INTO response(ExternalOrderId, EncryptedData) VALUES('${ExternalOrderId}', '${EncryptedData}');`
  );
}

loadTest()
  .then(() => {
    console.log(chalk.bold.bgGreenBright('SUCCESS'));
  })
  .catch(err => {
    console.error(chalk.bold.bgRedBright('ERROR:'), err);
  });
