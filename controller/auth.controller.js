const gravatar = require('gravatar');
const _ = require('lodash');
const Joi = require('@hapi/joi');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const sequelize = require('../utils/db');
const AppError = require('../utils/AppError');
const User = require('../models/user.model');
const Profile = require('../models/profile.model');
const Company = require('../models/company.model');

const signupValidator = reqBody => {
  const schema = Joi.object({
    firstName: Joi.string()
      .pattern(/^[a-z ,.'-]+$/i)
      .min(3)
      .max(30)
      .required(),
    lastName: Joi.string()
      .pattern(/^[a-z ,.'-]+$/i)
      .min(3)
      .max(30)
      .required(),
    password: Joi.string().pattern(/^[a-zA-Z0-9]{3,30}$/),
    email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }),
    address: Joi.string().min(5).max(1024),
    city: Joi.string().min(2).max(50),
    profile: Joi.string().min(3).max(30).required(),
    department: Joi.string().min(1).max(30).required()
  });

  return schema.validate(reqBody);
};

const loginValidator = reqBody => {
  const schema = Joi.object({
    email: Joi.string()
      .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
      .required(),
    password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')).required().messages({
      'string.pattern.base': 'Invalid password pattern'
    })
  });

  return schema.validate(reqBody);
};

exports.signupHandler = async (req, res, next) => {
  /**
   * IMPLEMENTING: Transaction
   */
  // const t = await sequelize.transaction();

  try {
    // Enforce validation
    const { error } = signupValidator(req.body);
    if (error) return next(new AppError(error.details[0].message, 400));

    const {
      firstName,
      lastName,
      email,
      password,
      city,
      address,
      profile: profileName,
      department
    } = req.body;
    const found = await User.findOne({ where: { email } });
    if (found) return next(new AppError('Email already registered.', 400));

    let url = gravatar.url(email, { s: '200', r: 'pg', d: '404' });
    url = url.replace('//', 'https://');
    const hashedPassword = await bcrypt.hash(password, 12);

    const result = await sequelize.transaction(async t => {
      // 1st db operation of transaction - t : create company
      const { dataValues: company } = await Company.create(
        {
          companyName: 'Vouchagram',
          address: '3rd Floor, IMM Building, Qutub Institutional area, New Delhi'
        },
        { transaction: t }
      );

      // 2nd db operation of transaction - t : create profile
      const { dataValues: profile } = await Profile.create(
        {
          companyId: company.id,
          profileName,
          department
        },
        { transaction: t }
      );

      // 3rd db operation of transaction - t: create user
      const { dataValues } = await User.create(
        {
          firstName,
          lastName,
          email,
          password: hashedPassword,
          city,
          address,
          profileImage: url,
          profileId: profile.id
        },
        { transaction: t }
      );

      const data = _.pick(dataValues, ['id', 'firstName', 'email', 'profileImage']);
      data.profile = {
        profileId: profile.id,
        profileName: profile.profileName,
        department: profile.department
      };
      data.company = {
        companyId: company.id,
        name: company.companyName,
        address: company.address
      };

      return data;
    });

    // await t.commit();
    res.status(200).json({
      status: 'success',
      message: 'Signup Successful',
      data: result
    });
  } catch (ex) {
    console.log(ex);

    // Rollback the transaction
    // await t.rollback();

    res.status(500).json({
      error: ex.message
    });
  }
};

exports.loginHandler = async (req, res, next) => {
  try {
    const { error } = loginValidator(req.body);
    if (error) return next(new AppError(error.details[0].message, 400));
    const { email, password } = req.body;
    const found = await User.findOne({ where: { email } });
    if (!found) return next(new AppError('Invalid email or password', 400));

    const { dataValues: user } = found;

    const correct = await bcrypt.compare(password, user.password);
    if (!correct) return next(new AppError('Invalid email or password', 400));

    const token = jwt.sign({ id: user.id, name: user.name }, process.env.JWT_SECRET, {
      expiresIn: '1d'
    });
    res.status(200).json({
      status: 'success',
      token,
      data: _.pick(user, ['firstName', 'email', 'profileImage'])
    });
  } catch (ex) {
    res.status(500).json({
      error: ex.message
    });
  }
};

exports.profileHandler = async (req, res, next) => {
  const profile = _.pick(req.user, [
    'id',
    'firstName',
    'lastName',
    'email',
    'address',
    'city',
    'profileImage'
  ]);
  res.status(200).json({
    status: 'success',
    data: {
      profile
    }
  });
};

exports.authCheck = async (req, res, next) => {
  try {
    if (!req.headers.authorization) return next(new AppError('No auth key provided', 401));
    const token = req.headers.authorization.split(' ')[1];
    if (!token) return next(new AppError('No auth key provided', 401));

    const decoded = jwt.verify(token, process.env.JWT_SECRET);

    const userExists = await User.findByPk(decoded.id);
    if (!userExists) return next(new AppError('User does not exist', 403));

    const { dataValues: user } = userExists;
    req.user = user;
    next();
  } catch (ex) {
    if (ex.name === 'SyntaxError') return next(new AppError('Tempered token', 403));
    if (ex.name === 'JsonWebTokenError') return next(new AppError('Invalid token', 403));
    next(new AppError('Something went wrong', 500));
  }
};
