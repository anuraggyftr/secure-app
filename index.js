const http = require('http');
const app = require('./app');
const sequelize = require('./utils/db');
const User = require('./models/user.model');
const Profile = require('./models/profile.model');
const Company = require('./models/company.model');

const server = http.createServer(app);

// Defining Associations
Profile.hasMany(User);
User.belongsTo(Profile, { constraints: true, onDelete: 'CASCADE' });
Company.hasMany(Profile);
Profile.belongsTo(Company, { constraints: true, onDelete: 'CASCADE' });

sequelize
  .sync()
  // .sync({ force: true })
  .then(() => {
    server.listen(3000, () => console.log(`server is ready at - localhost:3000`));
  })
  .catch(err => console.log(err));
